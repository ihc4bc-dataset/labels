# Labels for IHC4BC dataset
This repository contains the labels for [the IHC4BC dataset](https://ihc4bc.github.io/).


The labels might be updated, so pull frequently to access the most updated labels.

## Updates 

Aug 21, 2023: 12 pairs were removed due to hemosiderin/melanin.



